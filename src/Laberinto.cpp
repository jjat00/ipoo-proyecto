/*
  Programa: Laberinto.cpp
    Autor: Jaimen Aza  <jaimen.aza@correounivalle.edu.co>
           Esteban Jimenez <steban.jimenez@correounivalle.edu.co>
           Valentina Salamanca <salamanca.valentina@correounivalle.edu.co>
  Fecha creación: 2019-08-06
  Fecha última modificación: 2019-09-12
  Licencia: GNU-GPL
*/

#include "../include/Laberinto.h"

Laberinto::Laberinto(HallDeLaFama *hallDeLaFama, DispositivoEntradaSalida *dispositivo, int alto, int ancho, int gradoDificultad, int numeroPastillasRojas )
    : hallDeLaFama(hallDeLaFama), dispositivo(dispositivo), alto(alto), ancho(ancho), gradoDificultad(gradoDificultad),numeroPastillasRojas(numeroPastillasRojas)
{
  for (int horizontal = 0; horizontal < alto; horizontal++)
    for (int vertical = 0; vertical < ancho; vertical++)
      tablero.push_back(new Obstaculo(horizontal, vertical));

  //Se hacen los niveles 1 para fácil, 2 para medio y 3 para díficil
  switch (gradoDificultad)
  {
  case 1:
    numeroPastillasRojas = 3;
    vertices = 3;
    score = 140; // puntaje máximo nivel 1
    break;

  case 2:
    numeroPastillasRojas = 5;
    vertices = 10;
    score = 200; // puntaje máximo nivel 2
    break;

  case 3:
    numeroPastillasRojas = 6;
    vertices = 15;
    score = 300; // puntaje máximo nivel 3

    break;

  default:
    break;
  }

  numeroPasos = 0;
  int filaDestino = rand() % alto/3;
  filaNeo = rand() % alto;
  columnaNeo = 0;

  //Se crea un camino correcto que lleva a la salida de manera alealtoria junto a un camino incorrecto  para que el jugador se distraiga.
  vector<pair<int, int>> caminoInconrrecto1 = crearCaminoAlAzar(rand() % alto, 2, rand() % alto, ancho - 10, vertices / 3);
  for (int i = 0; i < caminoInconrrecto1.size(); i++)
    ponerObjetoEnLaberinto(new Vacia(caminoInconrrecto1[i].first, caminoInconrrecto1[i].second));
  
  vector<pair<int, int>> caminoCorrecto = crearCaminoAlAzar(filaNeo, columnaNeo, filaDestino, ancho - 1, vertices); 
  for (int i = 0; i < caminoCorrecto.size(); i++)
    ponerObjetoEnLaberinto(new Vacia(caminoCorrecto[i].first, caminoCorrecto[i].second));
  
  dispositivo->imprimirMensajeUsuario("PASTILLAS REQUERIDAS: " + to_string(numeroPastillasRojas), 1);

  int casillaPastillaRoja = caminoCorrecto.size() / (numeroPastillasRojas+ 2);
  int aumentarCasillaPastillaRoja = casillaPastillaRoja;
  vector<Objeto *> objetosARecoger;

  while(numeroPastillasRojas--)
  { 
    pair<int, int> coordenadas = caminar(caminoCorrecto[aumentarCasillaPastillaRoja].first, caminoCorrecto[aumentarCasillaPastillaRoja].second);
    if(coordenadas.first != -1 && coordenadas.second != -1)
    {
      Objeto *objeto = new Roja(coordenadas.first,coordenadas.second);
      ponerObjetoEnLaberinto(objeto);
      objetosARecoger.push_back(objeto);
    }
    aumentarCasillaPastillaRoja = aumentarCasillaPastillaRoja+casillaPastillaRoja ;    
  }

  ponerObjetoEnLaberinto(new Salida(filaDestino, ancho - 1));
  ponerObjetoEnLaberinto(new Morfeo(filaDestino, ancho - 2, objetosARecoger));
  ponerObjetoEnLaberinto(new Neo(filaNeo, columnaNeo, this));
}

Laberinto::~Laberinto()
{
  for (int elemento = 0; elemento < tablero.size(); elemento++)
    delete tablero[elemento];
}


bool Laberinto::jugar()
{
  dispositivo->imprimirMensajeUsuario("PASOS:  " + to_string(numeroPasos));

  Objeto *elemento = queHayEnCasilla(filaNeo, columnaNeo);
  Objeto *casillaAdelante, *casillaAnterior, *casillaArriba, *casillaAbajo;
  imprimir();

  int tecla = dispositivo->leerDireccion();
  switch (tecla)
  {
  case 0: //flecha arriba
    casillaArriba = queHayEnCasilla(filaNeo - 1, columnaNeo);
    if (casillaArriba->puedesQuitarteDeEnmedio(elemento) != 0)
    {
      numeroPasos++;
      casillaArriba->incrementaCoordenadas(1, 0);
      elemento->incrementaCoordenadas(-1, 0);
      borrarObjeto(casillaArriba);
      filaNeo -= 1;
    }
    break;

  case 1: //flecha abajo
    casillaAbajo = queHayEnCasilla(filaNeo + 1, columnaNeo);
    if (casillaAbajo->puedesQuitarteDeEnmedio(elemento) != 0)
    {
      numeroPasos++;
      casillaAbajo->incrementaCoordenadas(-1, 0);
      elemento->incrementaCoordenadas(1, 0);
      borrarObjeto(casillaAbajo);
      filaNeo += 1;
    }
    break;

  case 2: //flecha derecha
    casillaAdelante = queHayEnCasilla(filaNeo, columnaNeo + 1);
    if (casillaAdelante->puedesQuitarteDeEnmedio(elemento) != 0)
    {
      numeroPasos++;
      elemento->incrementaCoordenadas(0, 1);
      casillaAdelante->incrementaCoordenadas(0, -1);
      avisaQueLlegoALaSalida(casillaAdelante);        
      borrarObjeto(casillaAdelante);
      columnaNeo += 1;
    }
    break;

  case 3: //flecha izquierda
    casillaAnterior = queHayEnCasilla(filaNeo, columnaNeo - 1);
    if (casillaAnterior->puedesQuitarteDeEnmedio(elemento) != 0)
    {
      numeroPasos++;
      casillaAnterior->incrementaCoordenadas(0, 1);
      elemento->incrementaCoordenadas(0, -1);
      borrarObjeto(casillaAnterior);
      columnaNeo -= 1;
    }
    break;

  default:
    break;
    }

  return true;
}

//Determinar que hay en cada casilla del laberinto.

Objeto *Laberinto::queHayEnCasilla(int fila, int columna)
{
  for (int elemento = 0; elemento < tablero.size(); elemento++) 
    if (fila == tablero[elemento]->get_fila() and columna == tablero[elemento]->get_columna())
      return tablero[elemento];
  return 0;
}

bool Laberinto::avisaQueLlegoALaSalida(Objeto *objeto)
{
  if(objeto->comoTeLlamas() == "S")
  {
    puntaje();
    return true;
  }
  return false;
}

int Laberinto::puntaje()
{
  dispositivo->limpiarMensajesUsuario(0, 2);
  dispositivo->imprimirMensajeUsuario("Como te llamas?",0);
  string nombre = dispositivo->leerMensajeUsuario(1);
  hallDeLaFama->nuevoGanador(nombre, score - numeroPasos);
  hallDeLaFama->salvar();
  dispositivo->limpiarMensajesUsuario(0, 2);
  dispositivo->imprimirMensajeUsuario("--------------------------------------", 1);
  dispositivo->imprimirMensajeUsuario("            HALLDELAFAMA            ", 2);
  dispositivo->imprimirMensajeUsuario("--------------------------------------",3);
  dispositivo->imprimirMensajeUsuario("player  score",4);
  dispositivo->imprimirMensajeUsuario(hallDeLaFama->listaDeGanadores(10), 5);
  dispositivo->imprimirMensajeUsuario("¡GRACIAS POR JUGAR!",alto);
  return 0;
}

//Función que imprime el laberinto.

void Laberinto::imprimir()
{
  for (int elemento = 0; elemento < tablero.size(); elemento++)
    dispositivo->imprimirCasillaTablero(tablero[elemento]->get_fila(), tablero[elemento]->get_columna(), tablero[elemento]->comoTeLlamas());
}

//reemplazas lo que hay en casilla por vacia

Objeto *Laberinto::borrarObjeto(Objeto *objeto)
{
  int casilla = buscarCasilla(objeto);
  if (casilla == -1)
    return 0; // Error interno
  tablero[casilla] = new Vacia(tablero[casilla]->get_fila(), tablero[casilla]->get_columna());
  return tablero[casilla];
}

vector<pair<int, int>> Laberinto::crearCaminoAlAzar(int filaOrigen, int columnaNeo, int filaDestino, int columnaDestino, int vertices)
{
  vector<pair<int, int>> camino;

  int aumentarColumna = abs(columnaDestino - columnaNeo) / vertices;
  int avanzarFila1 = abs(filaDestino - filaOrigen);
  int avanzarFila2 = avanzarFila1+3;
  bool retrocederColumna = false;

  if (avanzarFila1 == 0)
  {
    avanzarFila1 = rand() % alto + 1;
    avanzarFila2 = avanzarFila1+3;
  }

  while (columnaNeo < columnaDestino - aumentarColumna)
  {
    if (filaOrigen < alto)
    {
      if (retrocederColumna == false)
      {
        for (int i = columnaNeo; i < columnaNeo + aumentarColumna; i++)
          camino.push_back(make_pair(filaOrigen, i));
        columnaNeo += aumentarColumna;
      }
      else
      {
        retrocederColumna = false;
        for (int i = columnaNeo; i > columnaNeo - aumentarColumna; i--)
          camino.push_back(make_pair(filaOrigen, i));
        columnaNeo -= aumentarColumna;

        for (int i = filaOrigen; i > filaOrigen - avanzarFila2; i--)
          camino.push_back(make_pair(i, columnaNeo));
        filaOrigen -= avanzarFila2;

        if (filaOrigen < 0)
        {
          filaOrigen = 0;
          aumentarColumna = aumentarColumna+3;
        }

        for (int i = columnaNeo; i < columnaNeo + aumentarColumna; i++)
          camino.push_back(make_pair(filaOrigen, i));
        columnaNeo += aumentarColumna;
      }

      for (int i = filaOrigen; i < alto; i++)
        camino.push_back(make_pair(i, columnaNeo));
      filaOrigen += avanzarFila1;
      
      if (filaOrigen > alto)
        filaOrigen = alto;
    }
    else
    {
      retrocederColumna = true;
      for (int i = columnaNeo; i < columnaNeo + aumentarColumna; i++)
        camino.push_back(make_pair(filaOrigen - 1, i));
      columnaNeo += aumentarColumna;

      for (int i = filaOrigen; i > filaOrigen - avanzarFila1; i--)
        camino.push_back(make_pair(i, columnaNeo));
      filaOrigen -= avanzarFila1;
    }
  }

  if (filaOrigen > filaDestino)
  {
    for (int i = filaOrigen; i > filaDestino; i--)
      camino.push_back(make_pair(i, columnaNeo));
    filaOrigen -= avanzarFila1;

    for (int i = columnaNeo; i < columnaDestino; i++)
      camino.push_back(make_pair(filaDestino, i));
    columnaNeo += aumentarColumna;
    
  }
  else
  {
    for (int i = filaOrigen; i < filaDestino; i++)
      camino.push_back(make_pair(i, columnaNeo));
    filaOrigen += avanzarFila1;

    for (int i = columnaNeo; i < columnaDestino; i++)
      camino.push_back(make_pair(filaDestino, i));
    columnaNeo += aumentarColumna;
  }
  return camino;
}

void Laberinto::vaciarCamino(vector<pair<int, int>> coordenadas, bool contarlongitudCamino)
{
}

int Laberinto::ponerObjetoEnLaberinto(Objeto *objeto)
{
  // Primero se averigua si hay algún otro objeto con esas coordenadas
  int casilla = buscarCasilla(objeto->get_fila(), objeto->get_columna());
  if (casilla != -1)
  {
    delete tablero[casilla];   // ... en cuyo caso, se borra el extremo donde apunta el puntero (pero no la casilla del vector)
    tablero[casilla] = objeto; // ... y se coloca allí mismo el nuevo objeto
  }
  else
    tablero.push_back(objeto); // En caso contrario, se añade el nuevo objeto al final
  return buscarCasilla(objeto); // Y se retorna la casilla donde quedó
}


int Laberinto::buscarCasilla(int fila, int columna)
{
  for (int elemento = 0; elemento < tablero.size(); elemento++)
    if (fila == tablero[elemento]->get_fila() and columna == tablero[elemento]->get_columna())
      return elemento;
  return -1;
}

int Laberinto::buscarCasilla(Objeto *objeto)
{
  for (int casilla = 0; casilla < tablero.size(); casilla++)
    if (tablero[casilla] == objeto)
      return casilla;
  return -1;
}

pair<int, int> Laberinto::caminar(int filaInicial, int columnaInicial)
{
  // Me aseguro de que la casilla esté Vacia:
  int casilla = buscarCasilla(filaInicial, columnaInicial);
  if (casilla != -1 && tablero[casilla]->puedesQuitarteDeEnmedio()) 
    return make_pair(filaInicial, columnaInicial);
  return make_pair(-1, -1); // Indica que no se encontró una casilla Vacia
}

void Laberinto::ordenar(int &menor, int &mayor, int &incremento)
{
}
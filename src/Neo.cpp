/*
  Programa: Neo.cpp
    Autor: Jaimen Aza  <jaimen.aza@correounivalle.edu.co>
           Esteban Jimenez <steban.jimenez@correounivalle.edu.co>
           Valentina Salamanca <salamanca.valentina@correounivalle.edu.co>
  Fecha creación: 2019-08-06
  Fecha última modificación: 2019-08-06
  Licencia: GNU-GPL
*/

#include "../include/Neo.h"

Neo::Neo(int fila, int columna,Laberinto *laberinto, bool obligatorio) : Objeto(fila, columna, "N", obligatorio), laberinto(laberinto)
{
}

Neo::~Neo()
{
    // No hay que hacer nada
}

Objeto *Neo::puedesQuitarteDeEnmedio(Objeto *quienPregunta)
{
  return this;
}
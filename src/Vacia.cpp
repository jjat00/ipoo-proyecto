/*
  Programa: Vacia.cpp
    Autor: Jaimen Aza  <jaimen.aza@correounivalle.edu.co>
           Esteban Jimenez <steban.jimenez@correounivalle.edu.co>
           Valentina Salamanca <salamanca.valentina@correounivalle.edu.co>
  Fecha creación: 2019-08-06
  Fecha última modificación: 2019-08-06
  Licencia: GNU-GPL
*/

#include "../include/Vacia.h"

Vacia::Vacia(int fila, int columna, bool obligatorio) : 
Objeto(fila,columna," ",obligatorio)
{
}

Vacia::~Vacia()
{
  // No hay que hacer nada
}

Objeto *Vacia::puedesQuitarteDeEnmedio(Objeto *quienPregunta)
{
    return this;
}


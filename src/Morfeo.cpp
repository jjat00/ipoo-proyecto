/*
  Programa: Morfeo.cpp
    Autor: Jaimen Aza  <jaimen.aza@correounivalle.edu.co>
           Esteban Jimenez <steban.jimenez@correounivalle.edu.co>
           Valentina Salamanca <salamanca.valentina@correounivalle.edu.co>
  Fecha creación: 2019-08-06
  Fecha última modificación: 2019-09-05
  Licencia: GNU-GPL
*/

#include "../include/Morfeo.h"

Morfeo::Morfeo(int fila, int columna, vector<Objeto *> pastillasRojasRequeridos)
    : Objeto(fila, columna, "M"), pastillasRojasRequeridos(pastillasRojasRequeridos)
{
}

Morfeo::~Morfeo()
{
}

Objeto *Morfeo::puedesQuitarteDeEnmedio(Objeto *quienPregunta)
{
    if (not quienPregunta)
        return 0;

    bool misionCumplida = false;
    if (quienPregunta->tienesObjeto(pastillasRojasRequeridos))
        misionCumplida = true;

    cout << "\n\n\n" + quienPregunta->hacerPregunta(string(misionCumplida ? "SI" : "NO") + " tienes las " + to_string(pastillasRojasRequeridos.size()) + " pastillas rojas requeridas. " + (misionCumplida ? "Puedes salir a la realidad" : "Regresa a Matrix"), false);

    if (misionCumplida)
        return this;
    return 0;
}

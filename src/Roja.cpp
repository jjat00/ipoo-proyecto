/*
  Archivo: Roja.cpp
  Autor: Ángel García Baños <angel.garcia@correounivalle.edu.co>
  Fecha creación: 2019-05-25
  Fecha última modificación: 2019-06-04
  Licencia: GNU-GPL
*/

#include "../include/Roja.h"

Roja::Roja(int fila, int columna) : Objeto(fila, columna, "R", true)
{
    // No hay que hacer nada
}

Roja::~Roja()
{
    // No hay que hacer nada
}

Objeto *Roja::puedesQuitarteDeEnmedio(Objeto *quienPregunta)
{
    if (not quienPregunta)
        return 0;
    return quienPregunta->cogeObjeto(this);
}

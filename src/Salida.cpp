/*
  Programa: Salida.cpp
    Autor: Jaimen Aza  <jaimen.aza@correounivalle.edu.co>
           Esteban Jimenez <steban.jimenez@correounivalle.edu.co>
           Valentina Salamanca <salamanca.valentina@correounivalle.edu.co>
  Fecha creación: 2019-08-06
  Fecha última modificación: 2019-08-06
  Licencia: GNU-GPL
*/

#include "../include/Salida.h"

Salida::Salida(int fila, int columna, bool obligatorio) : Objeto(fila, columna,"S", obligatorio)
{
}

Salida::~Salida()
{
  // No hay que hacer nada
}

Objeto *Salida::puedesQuitarteDeEnmedio(Objeto *quienPregunta)
{
    return this;
}

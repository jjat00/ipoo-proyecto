/*
  Programa: Obstaculo.cpp
    Autor: Jaimen Aza  <jaimen.aza@correounivalle.edu.co>
           Esteban Jimenez <steban.jimenez@correounivalle.edu.co>
           Valentina Salamanca <salamanca.valentina@correounivalle.edu.co>
  Fecha creación: 2019-08-06
  Fecha última modificación: 2019-08-06
  Licencia: GNU-GPL
*/

#include "../include/Obstaculo.h"

Obstaculo::Obstaculo(int fila, int columna, bool obligatorio) : Objeto(fila, columna, "[", obligatorio)
{
}
Obstaculo::~Obstaculo()
{
  // No hay que hacer nada
}


Objeto *Obstaculo::puedesQuitarteDeEnmedio(Objeto *quienPregunta)
{
    return 0;
}

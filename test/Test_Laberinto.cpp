/*
  Archivo: Test_Laberinto.cpp
  Autor: Ángel García Baños
  Email: angel.garcia@correounivalle.edu.co
  Fecha creación: 2019-08-02
  Fecha última modificación: 2019-08-02
  Versión: 0.1
  Licencia: GPL
*/

/*
  Utilidad: Un juego de salir de un laberinto.
  Historia: solo se va a probar el constructor y el destructor del Laberinto.
*/

#include "../include/Laberinto.h"
#include "../include/HallDeLaFama.h"
#include "../include/DispositivoTerminal.h"
#include <stdlib.h>
#include <vector>

int main(int numeroArgumentos, char *argumentos[])
{
  srand(time(0));

  int alto = 0;
  int ancho = 0;
  int gradoDeDificultad = 1;
  bool iniciarJuego = true;
  DispositivoEntradaSalida *menu = new DispositivoTerminal(1, 1,alto, ancho, 2, 2 + 2 + ancho, 12, 75);
  
  menu->imprimirMensajeUsuario("------------------------------------",0);
  menu->imprimirMensajeUsuario("            WELLCOME  ",1);
  menu->imprimirMensajeUsuario("------------------------------------",2);
  menu->imprimirMensajeUsuario("Hi, choose the difficulty level:", 3);
  menu->imprimirMensajeUsuario("1. Easy ",5);
  menu->imprimirMensajeUsuario("2. Medium ",6);
  menu->imprimirMensajeUsuario("3. Hard ",7);
  menu->imprimirMensajeUsuario("Input: ",9);

  string eleccionUsuario = menu->leerMensajeUsuario(10);

  switch (stoi(eleccionUsuario))
  {
  case 1:
    alto = 10;
    ancho = 30;
    gradoDeDificultad = 1;
    break;

  case 2:
    alto = 15;
    ancho = 45;
    gradoDeDificultad = 2;
    break;

  case 3:
    alto = 25;
    ancho = 70;
    gradoDeDificultad = 3;
    break;
  
  default: 

    menu ->imprimirMensajeUsuario ("Error, el nivel " + eleccionUsuario + " no existe", 12);
    menu ->imprimirMensajeUsuario (" ", 13);
    iniciarJuego = false;
    break;
  }

  if(iniciarJuego)
  {
    HallDeLaFama hallDeLaFama;
    DispositivoEntradaSalida *dispositivo = new DispositivoTerminal(2, 2, alto, ancho, 2, 2 + 2 + ancho, 12, 75);
    Laberinto laberinto(&hallDeLaFama, dispositivo, alto, ancho, gradoDeDificultad);

    while (laberinto.jugar())
    {
    }
    delete dispositivo;
    dispositivo = 0;
    return 0;
  }
  
  delete menu;
  menu = 0;
  return 0;
}
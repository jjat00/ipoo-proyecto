/*
  Archivo: Morfeo.h
  Autor: Ángel García Baños <angel.garcia@correounivalle.edu.co>
  Fecha creación: 2019-05-25
  Fecha última modificación: 2019-06-04
  Licencia: GNU-GPL
*/

/*
  Clase: Morfeo
  Atributos:
    - pastillasRojasRequeridos para dejarlo pasar (cada Trinity proporciona una pastilla Roja)
  Funcionalidades:
    - Si alguien intenta avanzar, primero le pregunta si tiene todos los tesoros, en cuyo caso responde que sí se puede.
  Relaciones:
    - Es un Objeto.
*/

#ifndef MORFEO_H
#define MORFEO_H

#include "Objeto.h"
//#include "../include/DispositivoTerminal.h"
#include <vector>
using namespace std;

class Morfeo : public Objeto
{
protected:
    vector<Objeto *> pastillasRojasRequeridos;

public:
  Morfeo(int fila, int columna, vector<Objeto *> pastillasRojasRequeridos);
  virtual ~Morfeo();
  /**
     * @brief un objeto externo me pregunta si puedo quitarme de enmedio, para avanzar encima de mi
     * @param quienPregunta es el objeto externo que hace la pregunta
     * @return true en caso de que sí pueda avanzar, para lo cual primero haré una pregunta a quienPregunta para saber si tiene suficientes pastillas Rojas.
     * @return retorna 0 en el caso de que no se pueda quitar de enmedio, debido a que quienPregunta no tiene suficientes pastillas Rojas. En caso contrario, retorna un puntero al propio Morfeo.
     * Si no hay nadie que pregunte (o sea, quienPregunta es 0) entonces no se hacen preguntas, por lo que solo las casillas Vacias pueden quitarse de enmedio.
     */
  virtual Objeto *puedesQuitarteDeEnmedio(Objeto *quienPregunta);
};

#else
class Morfeo; // Declaración adelantada
#endif

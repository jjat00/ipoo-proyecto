/*
  Archivo: Roja.h
  Autor: Ángel García Baños <angel.garcia@correounivalle.edu.co>
  Fecha creación: 2019-05-25
  Fecha última modificación: 2019-06-04
  Licencia: GNU-GPL
*/

/*
  Clase: Roja
  Atributos:
  Funcionalidades:
    - Si alguien intenta avanzar, responde que sí se puede, y le cede la propiedad del Tesoro.
  Relaciones:
    - Es un Objeto.
*/

#ifndef ROJA_H
#define ROJA_H

#include "Objeto.h"

class Roja : public Objeto
{
public:
    /**
     * @brief Roja crea una pastilla Roja en unas coordenadas dadas, y establece que sí es obligatorio cogerla para poder salir.
     * @param fila
     * @param columna
     */
    Roja(int fila, int columna);
    /**
     * @brief ~Roja destructor que no hace nada
     */
    virtual ~Roja();
    /**
     * @brief pregunta si se puede quitar esta pastilla Roja del camino. La respuesta es que sí, para lo cual basta cogerla.
     * @param quienPregunta
     * @return si quienPregunta recoge esta pastilla Roja, entonces puede avanzar, retornando la casilla Vacia que queda después de recogerse la pastilla Roja. En caso contrario, no puede avanzar, y retorna 0.
     */
    virtual Objeto *puedesQuitarteDeEnmedio(Objeto *quienPregunta);
};

#else
class Roja; // Declaración adelantada
#endif
